Adaptation du plugin [Item relations](https://omeka.org/classic/docs/Plugins/ItemRelations/) pour omeka classic


## Credits

Fork réalisé pour la plate-forme EMAN (ENS-CNRS-Sorbonne nouvelle) par Vincent Buard (Numerizen) avec le soutien du projet ANR Fiches de lecture Foucault : https://ffl.hypotheses.org. Voir les explications sur le site [EMAN](https://eman-archives.org/EMAN/item-relations)
